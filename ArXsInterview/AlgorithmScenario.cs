﻿using ArXsInterview.Infrastructure;

namespace ArXsInterview
{
    public class AlgorithmScenario : IScenario
    {
        public void Run(IFakeDatabase db)
        {
            // Gegeven een vierkante afbeelding (N x N)
            // Gevraagd: Draai de afbeelding 90 graden met de klok mee
            // Bonus vraag: Wat als we het geheugengebruik willen beperken? 
            //              Is er een algoritme dat in-place de rotatie uitvoert?

            var voorbeeld1 = new[]
            {
                new[] {1, 2},
                new[] {3, 4},
            };
            Draai(voorbeeld1);
            // voorbeeld1 ziet er nu zo uit:
            // new[]
            // {
            //     new[] {3, 1},
            //     new[] {4, 2},
            // };

            var voorbeeld2 = new[]
            {
                new[] {1, 2, 3},
                new[] {4, 5, 6},
                new[] {7, 8, 9},
            };
            Draai(voorbeeld2);
            // voorbeeld2 ziet er nu zo uit:
            // new[]
            // {
            //     new[] {7, 4, 1},
            //     new[] {8, 5, 2},
            //     new[] {9, 6, 3},
            // };
        }

        int[][] Draai(int[][] matrix)
        {
            // TODO
            return matrix;
        }
    }
}
