﻿using System.Collections.Generic;

namespace ArXsInterview.Infrastructure
{
    public interface IFakeDatabase
    {
        List<User> Users { get; set; }
        IKanBanBoardContext KanBanBoardContext { get; set; }
        IDesignContext DesignContext { get; set; }
    }
    public interface IKanBanBoardContext
    {
        List<KanBanBoardScenario.KanBanList> KanBanLists { get; set; }
    }
    public interface IDesignContext
    {
        List<DesignScenario.Resource> Resources { get; set; }
        List<DesignScenario.Location> Locations { get; set; }
        List<DesignScenario.LabourMeans> LabourMeans { get; set; }
        List<DesignScenario.Installation> Installations { get; set; }
        List<DesignScenario.PlanningInstance> PlanningInstances { get; set; }
        List<DesignScenario.Task> Tasks { get; set; }
        List<DesignScenario.Maintenance> Maintenances { get; set; }
        List<DesignScenario.Inspection> Inspections { get; set; }
    }
}
