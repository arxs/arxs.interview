﻿namespace ArXsInterview.Infrastructure
{
    public interface IScenario
    {
        void Run(IFakeDatabase db);
    }
}
