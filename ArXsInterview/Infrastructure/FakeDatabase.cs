﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using EntroBuilder;

namespace ArXsInterview.Infrastructure
{
    public class FakeDatabase : IFakeDatabase
    {
        public FakeDatabase()
        {
            Users = new Builder<User>()
                .Take(10)
                .ToList();
            
            KanBanBoardContext = new KanBanBoardContext(Users);
            DesignContext = new DesignContext();
        }

        public List<User> Users { get; set; }
        public IKanBanBoardContext KanBanBoardContext { get; set; }
        public IDesignContext DesignContext { get; set; }
    }

    public class KanBanBoardContext : IKanBanBoardContext
    {
        public KanBanBoardContext(List<User> users)
        {
            var dateTimeGenerator = Any.ValueBetween(DateTime.UtcNow.AddMonths(-6), DateTime.UtcNow.AddMonths(1));
            var textGenerator = new GibberishGenerator(File.ReadAllLines("trump-tweets.txt"));
            var userGenerator = new CustomGenerator<User>(random => users[random.NextBetween(0, users.Count-2)]);
            var timeSpentGenerator = new CustomGenerator<KanBanBoardScenario.TimeSpentOnItem>(
                random =>
                {
                    DateTime start = dateTimeGenerator.Next(random);
                    DateTime end = start.AddMinutes(random.NextBetween(1, 500));
                    return new KanBanBoardScenario.TimeSpentOnItem(userGenerator.Next(random), start, end);
                });
            
            KanBanLists = new Builder<KanBanBoardScenario.KanBanList>()
                .For(userGenerator)
                .For(textGenerator)
                .For(Any.ValueIn(users.ToArray()))
                .For(Any.ValueBetween(0M, 1000M))
                .For(dateTimeGenerator)
                .For(timeSpentGenerator)
                .Take(100)
                .ToList();
        }

        public List<KanBanBoardScenario.KanBanList> KanBanLists { get; set; }
    }
    public class DesignContext : IDesignContext
    {
        public DesignContext()
        {
            Resources = new List<DesignScenario.Resource>();
            Locations = new List<DesignScenario.Location>();
            LabourMeans = new List<DesignScenario.LabourMeans>();
            Installations = new List<DesignScenario.Installation>();
            PlanningInstances = new List<DesignScenario.PlanningInstance>();
            Tasks = new List<DesignScenario.Task>();
            Maintenances = new List<DesignScenario.Maintenance>();
            Inspections = new List<DesignScenario.Inspection>();
        }

        public List<DesignScenario.Resource> Resources { get; set; }
        public List<DesignScenario.Location> Locations { get; set; }
        public List<DesignScenario.LabourMeans> LabourMeans { get; set; }
        public List<DesignScenario.Installation> Installations { get; set; }
        public List<DesignScenario.PlanningInstance> PlanningInstances { get; set; }
        public List<DesignScenario.Task> Tasks { get; set; }
        public List<DesignScenario.Maintenance> Maintenances { get; set; }
        public List<DesignScenario.Inspection> Inspections { get; set; }
    }
}
