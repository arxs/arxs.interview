﻿using System;
using System.Collections.Generic;
using System.Linq;
using EntroBuilder;
using Markov;

namespace ArXsInterview.Infrastructure
{
    public class GibberishGenerator : IGenerator<string>
    {
        readonly MarkovChain<string> _chain;
        public GibberishGenerator(IEnumerable<string> quotes)
        {
            _chain = new MarkovChain<string>(1);
            foreach (var quote in quotes)
            {
                _chain.Add(quote
                    .Replace("\"","")
                    .Replace("&amp;","&")
                    .Split(' ', '\t', StringSplitOptions.RemoveEmptyEntries));
            }
        }

        public string Next(Random random)
        {
            return string.Join(" ", _chain.Chain(random).Take(random.NextBetween(3, 12)));
        }
        object IGenerator.Next(Random random)
        {
            return Next(random);
        }
    }
}
