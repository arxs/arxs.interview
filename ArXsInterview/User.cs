﻿using System;

namespace ArXsInterview
{
    public class User
    {
        User() { }
        public User(Guid id, string name)
        {
            Id = id;
            Name = name;
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
