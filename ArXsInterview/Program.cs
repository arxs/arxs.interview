﻿using ArXsInterview.Infrastructure;

namespace ArXsInterview
{
    class Program
    {
        static void Main(string[] args)
        {
            // GEGEVEN
            // -------
            // - FakeDatabase is een class met willekeurig gegenereerde testdata die een database simuleert
            //   Er zijn een aantal tabellen
            //   - Users stelt een gedeelde gebruiker tabel voor
            //   - De KanBanBoardScenario tabellen
            //   - De DesignScenario tabellen
            // - IFakeDatabase is een interface voor FakeDatabase
            // - IScenario is een interface voor scenarios
            // - AlgorithmScenario is een scenario waarin vragen te beantwoorden zijn
            // - KanBanBoardScenario is een scenario waarin vragen te beantwoorden zijn
            // - DesignScenario is een scenario waarin vragen te beantwoorden zijn
            // - GibberishGenerator is class die helpt test data te genereren

            // TECHNISCHE ACHTERGROND
            // ----------------------
            // We maken gebruik van LINQ-to-objects, een deel van de BCL die helpt om bewerkingen te doen op collections 
            // Je kan dit vergelijken met SQL in een relationele database waar je declaratief beschrijft hoe je data wil
            //   ophalen, filteren, groeperen, sorteren en of omvormen naar de structuur die jij wil
            
            // Dictionary<TKey, TValue> is de dotnet implementatie van een HashMap<,>: een key-value datastructuur

            // TODO: Kan je uitleggen wat een interface is?
            // TODO: Zie je een nut om de scenario classes (KanBanBoardScenario en PolymorphismScenario) de interface IScenario te laten implementeren gegeven de code hieronder?
            var scenarios = new IScenario[]
            {
                new AlgorithmScenario(),
                new KanBanBoardScenario(),
                new DesignScenario(),
            };
            
            // TODO: Waarom instantiëren we de database niet in het scenario?
            var db = new FakeDatabase();

            foreach (var scenario in scenarios)
            {
                scenario.Run(db);
            }
        }
    }
}
