﻿using System;
using System.Collections.Generic;
using System.Linq;
using ArXsInterview.Infrastructure;

namespace ArXsInterview
{
    public class KanBanBoardScenario : IScenario
    {
        public void Run(IFakeDatabase db)
        {
            // TODO: Wat doet dit en hoe werkt het?
            var results1 = db.KanBanBoardContext.KanBanLists
                .SelectMany(x => x.Items.SelectMany(i => i.TimeSpent))
                .GroupBy(i => i.User.Id)
                .ToDictionary(g => g.Key, g => g.Sum(i => (i.To - i.From).TotalHours));

            // TODO: Wat kan je vertellen over een Dictionary<TKey, TValue> in vergelijking tot een List<T>?

            // TODO: Kan je een overzicht ophalen van totale ActualCost per lijst (enkel de titel van de lijst is relevant)?

            // Bonus vraag: Hoe zouden we kunnen achterhalen welke gebruiker nog geen werk heeft verricht (waarvoor TimeSpentOnItem records ontbreken)?
        }

        /// <summary>
        /// KanBanList stelt een todo-lijstje op een kanban bord voor (zoals bijvoorbeeld Trello)
        /// </summary>
        public class KanBanList
        {
            KanBanList() { }
            public KanBanList(string title, IEnumerable<KanBanListItem> items)
            {
                Title = title;
                Items = items?.ToList() ?? new List<KanBanListItem>();
            }

            public string Title { get; set; }
            public List<KanBanListItem> Items { get; set; }
        }

        public class KanBanListItem
        {
            public string Description { get; set; }
            public DateTime? TargetDate { get; set; }
            public decimal? EstimatedCost { get; set; }

            public DateTime? CompletedAt { get; private set; }
            public decimal ActualCost { get; private set; }

            public List<TimeSpentOnItem> TimeSpent { get; set; }
            public List<User> AssignedUsers { get; set; }

            KanBanListItem() { }
            public KanBanListItem(string description, DateTime? targetDate = null, decimal? estimatedCost = null)
            {
                Description = description;
                TargetDate = targetDate;
                EstimatedCost = estimatedCost;
                TimeSpent = new List<TimeSpentOnItem>();
                AssignedUsers = new List<User>();
            }

            public KanBanListItem RecordTimeSpent(User user, DateTime start, DateTime end)
            {
                TimeSpent.Add(new TimeSpentOnItem(user, start, end));
                return this;
            }

            public KanBanListItem AssignUser(User user)
            {
                AssignedUsers.Add(user);
                return this;
            }
            public KanBanListItem UnassignUser(User user)
            {
                AssignedUsers.Remove(user);
                return this;
            }

            public KanBanListItem Complete(DateTime completedAt, decimal actualCost)
            {
                CompletedAt = completedAt;
                EstimatedCost = EstimatedCost ?? actualCost;
                ActualCost = actualCost;

                return this;
            }
        }

        public class TimeSpentOnItem
        {
            TimeSpentOnItem() { }
            public TimeSpentOnItem(User user, DateTime from, DateTime to)
            {
                User = user;
                From = from;
                To = to;
            }

            public DateTime From { get; set; }
            public DateTime To { get; set; }
            public User User { get; set; }
        }
    }
}
