﻿using System;
using System.Collections.Generic;
using ArXsInterview.Infrastructure;

namespace ArXsInterview
{
    public class DesignScenario : IScenario
    {
        public void Run(IFakeDatabase db)
        {
            // db.DesignContext is de data store. Deze mag worden aangepast om onderstaande vragen gemakkelijker te kunnen
            // beantwoorden.

            // TODO: Gegeven onderstaande beginsituatie, pas Task/Maintenance/Inspection/PlanningInstance aan zodat
            //       - we Task/Maintenance/Inspection kunnen inplannen
            //       - we in 1 query op db.DesignContext een overzicht kunnen bekomen van de
            //         planning (PlanningInstance + Task/Maintenance/Inspection) van vandaag
            //       - we in 1 query op db.DesignContext een overzicht kunnen bekomen van de
            //         planning (PlanningInstance + Task/Maintenance/Inspection) voor een specifieke User
            //       - we code duplicatie beperken voor de 3 verschillende soorten planbare classes
            
            // Bonus vraag: Toon dat het datamodel de vragen kan beantwoorden door bovenstaande queries te implementeren
        }

        // ========== TER INFO ==========
        public abstract class Resource
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
        }

        /// <summary>
        /// Locatie bv. een lokaal in een gebouw, een deel van een productiehal, ...
        /// </summary>
        public class Location : Resource {}

        /// <summary>
        /// Arbeidsmiddel bv. een boormachine, ...
        /// </summary>
        public class LabourMeans : Resource {}

        /// <summary>
        /// Uitrusting/installatie: bv. een machine, een computer, een voertuig, ...
        /// </summary>
        public class Installation : Resource {}

        // ========== /TER INFO ==========

        // ========== AAN TE PASSEN ==========
        public class Task
        {
            public Guid Id { get; set; }
            public string Type { get; set; }
            public string Title { get; set; }
            /// <summary>
            /// Een taak kan uitgevoerd worden op een "middel" (machine, noodverlichting, voertuig)
            /// Deze relatie is optioneel. Vandaar kan bijhorende ResourceId null zijn.
            /// </summary>
            public Resource Resource { get; set; }
            public Guid? ResourceId { get; set; }

            public decimal? EstimatedCost { get; set; }
        }

        public class Maintenance
        {
            public Guid Id { get; set; }
            public string Type { get; set; }
            /// <summary>
            /// Een onderhoud wordt steeds uitgevoerd op een "middel" (machine, noodverlichting, voertuig)
            /// Deze relatie (met bijhorende ResourceId) moet steeds ingevuld zijn.
            /// </summary>
            public Resource Resource { get; set; }
            public Guid ResourceId { get; set; }

            public decimal? EstimatedCost { get; set; }
        }

        public class Inspection
        {
            public Guid Id { get; set; }
            public string Type { get; set; }
            /// <summary>
            /// Een inspectie wordt steeds uitgevoerd op een "middel" (machine, noodverlichting, voertuig)
            /// Deze relatie (met bijhorende ResourceId) moet steeds ingevuld zijn.
            /// </summary>
            public Resource Resource { get; set; }
            public Guid ResourceId { get; set; }

            public decimal? EstimatedCost { get; set; }
        }

        public class PlanningInstance
        {
            public DateTime From { get; set; }
            public DateTime To { get; set; }
            public List<User> AssignedUsers { get; set; }
        }
        // ========== /AAN TE PASSEN ==========
    }
}
